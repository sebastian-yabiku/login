'use strict';
/*GULP-PATH*/
var dir = '.';
module.exports = {
	dev : {
		sassFile : dir + '/front-end/scss/**/*.scss',
		sassPath : dir + '/front-end/scss/',
		imagePath : dir + '/front-end/image/',
		imageFile : dir + '/front-end/image/**/*.*'
	} ,
	prod : {
		css  : dir + '/static/css/',
		imagePath : dir + '/static/image/'
	}
};
