'use strict';
var gulp = require('gulp'),
	spritesmith = require('gulp.spritesmith');

module.exports = function(options){

	var config = {
		task : {
			default : "sprite"
		},
		taskWatch : {
			default : "sprite:watch"
		},
		dev : {
			default : {
				image : options.dev.imagePath + 'design',
				sass : options.dev.sassPath + 'app'
			}
		},
		prod : {
			default : {
				image : options.prod.imagePath + 'design/'
			}
		}
	};

	gulp.task(config.task.default, spriteTaskDefault);

	function spriteTaskDefault()
	{
		var spriteData = gulp.src(config.dev.default.image + '/sprite/*.png').pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '_sprite.scss',
			imgPath : '../../../static/image/design/sprite.png',//config.prod.default.image + 'sprite.png',
			padding: 10
		}));
		
		spriteData.img.pipe(gulp.dest(config.dev.default.image));
		spriteData.css.pipe(gulp.dest(config.dev.default.sass));
	}
};
